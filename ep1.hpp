#ifndef IMAGEM_H
#define IMAGEM_H
#pragma
#include <iostream>

using namespace std;

class Imagem {
	private:
		int Altura;
		int Largura;
		int Nivelcinza;
		int *Pixels;
	public:
	
		Imagem();
		Imagem(int Altura, int Largura, int Nivelcinza);
		void ReadImagem();
		void SalvarOutraImagem();
		void NegativoImagem();		
		int getAltura();
		int getLargura();
		int getNivelcinza();
		int getValordeUmPixel(int linha, int coluna);
		void setValordeUmPixel(int linha, int coluna, int value);
		
			
};

#endif