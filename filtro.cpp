#include "filtro.hpp"
#include "ep1.cpp"
using namespace std;

Filtro::Filtro(){	
	
}

void Filtro::setDiv(int div) {
	this->div = div;
}

void Filtro::setSize(int size) {
	this->size = size;
}

int Filtro::getDiv() {
	return div;
}

int Filtro::getSize() {
	return size;
}
void Filtro::Sharpen (Imagem &umaImagem, int div, int size) {
		//matriz que vai passar pela imagem
		int sharpen[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};

		int Altura, Largura, value, i,j;
		
		
	
		Altura = 512;
		Largura = 512;
		int *m = new int[Altura*Largura];
		
		//passando a matriz pela imagem		
		for (i=size/2; i < Altura-size/2; i++)
		{
			for (j = size/2; j < Largura-size/2; j++)
			{
				value = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						value += sharpen[(x+1)+ size*(y+1)] *
						//pegando o valor de um pixel da imagem sem filtro
					    umaImagem.getValordeUmPixel(i+x, y+j);
					
					}
				}
				
				value /=div;
				
			
					
				value= value < 0 ? 0 : value;
				value=value >255 ? 255 : value;
				
				m[i+Largura*j] = value;
			}
		}
		//salvando os novos valores de pixels
		for(i=0;i<Altura; i++)
			for(j=0; j<Largura; j++)
				umaImagem.setValordeUmPixel(i, j, m[i+Largura*j]);

}
void Filtro::Smooth (Imagem &umaImagem, int div, int size) {
		
		int smooth[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};

		int Altura, Largura, value, i,j;
		
		
	
		Altura = 512;
		Largura = 512;
		int *m = new int[Altura*Largura];
		
		for (i=size/2; i < Altura-size/2; i++)
		{
			for (j = size/2; j < Largura-size/2; j++)
			{
				value = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						value += smooth[(x+1)+ size*(y+1)] *
					    umaImagem.getValordeUmPixel(i+x, y+j);
					
					}
				}
				
				value /=div;
				
			
					
				value= value < 0 ? 0 : value;
				value=value >255 ? 255 : value;
				
				m[i+Largura*j] = value;
			}
		}
		
		for(i=0;i<Altura; i++)
			for(j=0; j<Largura; j++)
				umaImagem.setValordeUmPixel(i, j, m[i+Largura*j]);

}
