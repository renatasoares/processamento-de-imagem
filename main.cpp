#include "filtro.cpp"

using namespace std;

int main (){
	int opcao;
	//Definindo o objeto lena e lena2
	Imagem lena;
	Filtro lena2;
	lena.ReadImagem();
	//menu para o usuario
	cout <<"Escolha o filtro desejado: "<<endl;
	cout<< "1-Negativo\n2-Smooth\n3-Sharpen"<<endl;
	cin>>opcao;
	if (opcao==1){
		lena.NegativoImagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	if (opcao==2){
		lena2.setDiv(9);
		lena2.setSize(3);
		lena2.Smooth(lena, lena2.getDiv(), lena2.getSize());
		lena.SalvarOutraImagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	if (opcao==3){
		lena2.setDiv(1);
		lena2.setSize(3);
		lena2.Sharpen(lena, lena2.getDiv(), lena2.getSize());
		lena.SalvarOutraImagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	else {
		cout<< "Opcao invalida"<<endl;
	}
	
	
	
	return 0;
}