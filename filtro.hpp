#ifndef FILTRO_H
#define FILTRO_H
#pragma
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

#include "ep1.hpp"

using namespace std;

class Filtro : public Imagem{
	private:
		int div;
		int size;
	public:
		Filtro();
		void setDiv(int div);
		void setSize(int size);
		int getDiv();
		int getSize();
		void Sharpen(Imagem &umaImagem,int div, int size);
		void Smooth(Imagem &umaImagem, int div, int size);

};

#endif