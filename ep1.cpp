#include "ep1.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 
#include <string.h>

using namespace std;

Imagem::Imagem() {
	Altura = 0;
	Largura = 0;
	Nivelcinza = 0;
	Pixels = 0;
}

void Imagem::ReadImagem() {
	int i, j;
	unsigned char *Imagem;
	char numeroMagico[3], altura[4], largura[5], nivelCinza[10];
	char teste[3]="P5";
	char comentario[100];
	int Altura, Largura, Nivelcinza;
	
	//alocando memória para salvar os Pixels
	Pixels = new int [512*512+513];
	
	ifstream file ("lena.pgm");

	//lendo numero magico
	file.getline (numeroMagico, 3);

	
	//testando imagem para verificar que e pgm
	if (strcmp(numeroMagico, teste)!=0){
		cout << "A imagem nao e um pgm" << endl;
		exit(1);
	}
    
	//lendo o comentario
	file.getline (comentario, 100);

	//lendo altura/largura/nivel maximo de cinza
	file.get(altura, 4);
	file.getline(largura, 5);
	file.getline(nivelCinza, 10);
		
	//transformando de string para int
	Altura=atoi(altura);
	Largura=atoi(largura);
	Nivelcinza=atoi(nivelCinza);
	
	//armazenando os dados recebidos
	this->Altura = Altura;
	this->Largura = Largura;
	this->Nivelcinza = Nivelcinza;

	//alocando memoria para os Pixels ainda em char
    	Imagem=(unsigned char *) new unsigned char [Altura*Largura+512];

	file.read( reinterpret_cast<char *>(Imagem), (Altura*Largura)*sizeof(unsigned char));
	file.close();

	long long auxiliar;

	//mudando os dados recebidos em char para int
	for(i=0; i<Altura; i++){
        	for(j=0; j<Largura; j++) {
            		auxiliar= (int)Imagem[i*Largura+j];
            		Pixels[i*Largura+j] = auxiliar;  
			
        	}
	
	}

}

int Imagem::getAltura() {
	return Altura;	
}

int Imagem::getLargura() {
	return Largura;
}

int Imagem::getNivelcinza() {
	return Nivelcinza;
}

void Imagem::SalvarOutraImagem() {
	int i, j;
	long long auxiliar;
	unsigned char *Imagem;
	ofstream outfile("lenaModificado.pgm");
	
	//alocando memoria para os Pixels da imagem
	Imagem=(unsigned char *) new unsigned char [512*512+513];

	//transformando os dados em int recebidos para char
	for(i=0; i<512; i++){
        	for(j=0; j<512; j++){
			auxiliar = Pixels[i*512+j];
			Imagem[i*512+j]=(unsigned char)auxiliar;
        	}
    	}
	//escrevendo no arquivo o numero magico,altura,largura,comentario e nivel maximo do cinza
	outfile << "P5" << endl;
	outfile << "512 512" <<endl;
	outfile<< "#Create by Renata Soares"<<endl;
	outfile << 255 << endl;
	
	//escrevendo os pixels como char
	
	outfile.write(reinterpret_cast<char *>(Imagem), (512*512)*sizeof(unsigned char));
	
	outfile.close();
}
void Imagem::NegativoImagem() {
	int i, j;
	long long auxiliar;
	unsigned char *Imagem;
	ofstream outfile("lenaNegativo.pgm");
	
	Imagem=(unsigned char *) new unsigned char [512*512+513];

	for(i=0; i<512; i++){
        	for(j=0; j<512; j++){
			//subtraindo o nivel de cinza pixel por pixel
			auxiliar = 255 - Pixels[i*512+j];
			Imagem[i*512+j]=(unsigned char)auxiliar;
        	}
    	}
	
	outfile << "P5" << endl;
	outfile << "512 512" <<endl;
	outfile<< "#Create by Renata Soares"<<endl;
	outfile << 255 << endl;
	
	outfile.write(reinterpret_cast<char *>(Imagem), (512*512)*sizeof(unsigned char));
	outfile.close();
}
//retorna o valor de um pixel recebido por vez
int Imagem::getValordeUmPixel(int linha, int coluna)
{
	int sizeY = getAltura();
    return Pixels[linha*sizeY+coluna];
}

//salva o valor recebido de um pixel por vez
void Imagem::setValordeUmPixel(int linha, int coluna, int value)
{
	int sizeY = getLargura();
	
    Pixels[linha*sizeY+coluna] = value;
}